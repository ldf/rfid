/**CFile*****************************************************************

  FileName    [random.c]

  Synopsis    [Implementation file for the "random" data structure.]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "random.h"


#define MAXBUF 40

/**Function********************************************************************
  Synopsis           [Initialize map data structure.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int random_init(random_t **r){
	Malloc((void *)r, sizeof(random_t));
	return 0;
}

//TODO implement correctly this function
float random_uniform_float_gen(float min, float max, int *seed){
	return r4_uniform_ab(min, max, seed);
}

float random_uniform_int_gen(int min, int max, int *seed){
	return i4_uniform_ab(min, max, seed);
}

/**Function********************************************************************
  Synopsis           [Read the input file and sets the parameters.]
  Description        []
  SideEffects        [In case x and y are both equals to 0, the calling function
  	  	  	  	  	  should set manually these values.
  	  	  	  	  	  This is done so that this can be equals to the values of the
  	  	  	  	  	  map.]
  SeeAlso            []
******************************************************************************/
int random_input_file_read(random_t **r, int model_on_off){
	FILE *ifp;
	char *ifn = strdup("/Users/ldf/Documents/workspace/rfid/src/etc/seeds");
	char buf[MAXBUF+1], parser[MAXBUF+1];
	ifp = fopen(ifn, "r");
	if(ifp==NULL){fprintf(stderr, "Cannot open seeds file\n"); return -1;}

	while(fgets(buf, MAXBUF, ifp)!=NULL){
		sscanf(buf, "%s", parser);
	    switch(buf[0]){
	    case 'p':
	    	if(!strcmp(parser, "position")) {
	    		sscanf(buf, "%*s %d", &((*r)->seed_position));
	    		fscanf(ifp, "%f %f\n", &((*r)->position_x), &((*r)->position_y));
	    	}
	    	break;
	    case 'o':
	    	 if(!strcmp(parser, "off_time") && model_on_off==1) {
	    	 	sscanf(buf, "%*s %d", &((*r)->seed_time_off));
	    	 	fscanf(ifp, "%f %f\n", &((*r)->time_off_min), &((*r)->time_off_max));
	    	 }else{
	    		 (*r)->seed_time_off = -1;
	    		 (*r)->time_off_min = -1;
	    		 (*r)->time_off_max = -1;
	    	 }
	    	 break;
	    case 's':
	    	if(!strcmp(parser, "speed")){
	    		sscanf(buf, "%*s %d", &((*r)->seed_speed));
	    		fscanf(ifp, "%f %f\n", &((*r)->speed_min), &((*r)->speed_max));
	    	}
	    	if(!strcmp(parser, "state")){
	    		sscanf(buf, "%*s %d", &((*r)->seed_state));
	    	}
	    	break;
	    case 't':
	    	if(!strcmp(parser, "time")){
	    		sscanf(buf, "%*s %d", &((*r)->seed_wait_time));
	    		fscanf(ifp, "%f %f\n", &((*r)->wait_time_min), &((*r)->wait_time_max));
	    	}

	    	if(!strcmp(parser, "tag_distance")) {
	    		sscanf(buf, "%*s %d", &((*r)->seed_tag_distance));
	    		fscanf(ifp, "%f %f\n", &((*r)->tag_distance_min), &((*r)->tag_distance_max));
	    	}
	    	break;
	    }
	}
	return 0;
}

void random_coord(random_t *r, coord_t **c, int *seed){
	(*c)->x = random_uniform_float_gen(0, r->position_x, seed);
	(*c)->y = random_uniform_float_gen(0, r->position_y, seed);
	return;
}
void random_speed(random_t *r, float *speed, int *seed){
	(*speed) = random_uniform_float_gen(r->speed_min, r->speed_max, seed);
	return;
}
void random_wait_time(random_t *r, float *wait_time, int *seed){
	(*wait_time) = random_uniform_float_gen(r->wait_time_min, r->wait_time_max, seed);
	return;
}
void random_tag_distance(random_t *r, float *tag_distance, int *seed){
	(*tag_distance) = random_uniform_float_gen(r->tag_distance_min, r->tag_distance_max, seed);
	return;
}
void random_time_on_off(random_t *r, float *wait_time, int *state, int *seed_time_on, int *seed_state){
	(*wait_time) = random_uniform_float_gen(r->wait_time_min, r->wait_time_max, seed_time_on);
//	(*state) = random_uniform_int_gen(0, 1, seed_state);
	(*state) = 1; //always on
	return;
}

/**Function********************************************************************
  Synopsis           [Print the parameters of the random data structure.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
void random_print(random_t *r){
	fprintf(stdout, "Random seeds configured:\n");
	fprintf(stdout, "position %d\n", r->seed_position);
	fprintf(stdout, "%.2f %.2f\n", r->position_x, r->position_y);
	fprintf(stdout, "speed %d\n", r->seed_speed);
	fprintf(stdout, "%.2f %.2f\n", r->speed_min, r->speed_max);
	fprintf(stdout, "time %d\n", r->seed_wait_time);
	fprintf(stdout, "%.2f %.2f\n", r->wait_time_min, r->wait_time_max);
	fprintf(stdout, "End of random seeds configured\n");
	return;
}
