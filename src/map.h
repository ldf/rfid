/**CHeaderFile*****************************************************************

  FileName    [map.h]�

  Synopsis    [Header file for the "map" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef MAP_H_
#define MAP_H_

/*---------------------------------------------------------------------------*/
/* Nested includes                                                           */
/*---------------------------------------------------------------------------*/
#include "node.h"
#include "param.h"
#include "utility.h"
#include "random.h"
#include "uniform.h"
//#include "inter.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef struct map map_t;

struct map{
	node_t **nodes;
	int num_on, num_off;
	float time;
	param_t *p;
	random_t *r;
	//inter_t *i;
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
int map_init(map_t **m);
int map_first_setup(random_t *r, map_t **m);
int map_input_file_read(map_t **m);
int map_update(random_t *r, map_t **m, float time);
int map_print(map_t *m); //hobby function


#endif /* MAP_H_ */
