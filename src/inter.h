/**CHeaderFile*****************************************************************

  FileName    [inter.h]�

  Synopsis    [Header file for the "inter" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef INTER_H_
#define INTER_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "param.h"
#include "map.h"
#include "utility.h"


#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

typedef struct inter inter_t;

struct inter{
	int *v;
	int num_dev;

};

int inter_init(inter_t **i);
int inter_first_setup(inter_t **i, int num_dev);
int inter_update(map_t *m, inter_t **i);
void inter_print(inter_t *i);

float inter_dist_a_b(float x0, float x1, float y0, float y1);

int cl_inter_update(map_t *m, inter_t **i);

#endif /* INTER_H_ */
