/**CHeaderFile*****************************************************************

  FileName    [utility.h]�

  Synopsis    [Header file for the "utiliy" functions]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef UTILITY_H_
#define UTILITY_H_

#include <stdio.h>
#include <stdlib.h>

#define YES  1
#define NO   0

#define ON	 1
#define OFF  0

/* This is a global parameter that describe the verbosity of the entire program
 * this is generally used before a function that need to print some information. */
#define VERBOSITY  0 // [range: 0-5]
#define VISUAL    NO

int Malloc(void **p, int size);

#endif /* UTILITY_H_ */
