 /**CHeaderFile*****************************************************************

  FileName    [visual.h]�

  Synopsis    [Header file for the "visual" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef VISUAL_H_
#define VISUAL_H_


#include <stdio.h>
#include <stdlib.h>
#include "map.h"
#include "inter.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define THRESHOLD 1000

map_t * global_map;
inter_t *global_inter;


void visual_render(void);
void visual_reshape(int w, int h);
void visual_simulation();

#endif /* VISUAL_H_ */
