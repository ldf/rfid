/**CHeaderFile*****************************************************************

  FileName    [visual.c]�

  Synopsis    [Header file for the "visual" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "visual.h"
#include "utility.h"

int k=0;
int current_time=0;

void visual_reshape(int w, int h){
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
}

void visual_render(void){
	int i;
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0,0.0,0.0);
    glLineStipple(1, 0x0111);
    for(i=0; i<global_map->p->num_dev; i++){
    	glPointSize(9.8);
    	glBegin(GL_POINTS);
    	if(global_map->nodes[i]->state == OFF) glColor4f(0, 0, 0, 1);
    	else if(global_inter->v[i] == 0) glColor4f(0.12, 2.94, 0.12, 1);
    	else if(global_inter->v[i] == 1) glColor4f(2.3, 0.12, 0.0, 1);

    	glVertex2f(global_map->nodes[i]->coord->x, global_map->nodes[i]->coord->y);
    	glEnd();

    	glBegin(GL_LINES);
    	glColor4f(0.0, 0.0, 0.0, 1);
    	glVertex2f(global_map->nodes[i]->coord->x, global_map->nodes[i]->coord->y);
    	glVertex2f(global_map->nodes[i]->move->coord->x, global_map->nodes[i]->move->coord->y);
    	glEnd();

    	glPointSize(1.8);
    	glBegin(GL_POINTS);
    	glColor4f(0.0, 0.0, 0.0, 1);
    	glVertex2f(global_map->nodes[i]->move->coord->x, global_map->nodes[i]->move->coord->y);
    	glEnd();
    }

    glDisable(GL_LINE_STIPPLE);
    glutSwapBuffers();
}

void visual_simulation(){
	if(glutGet(GLUT_ELAPSED_TIME) - current_time >THRESHOLD){
	if(k<global_map->p->num_it){
		map_update(global_map->r, &global_map, global_map->p->sim_time);
		if(VERBOSITY>1) map_print(global_map);
		inter_update(global_map, &(global_inter));
		if(VERBOSITY>-1)inter_print(global_inter);
		glutPostRedisplay();
		k++;
	}else{
		glutTimerFunc(100, NULL, 0);
	}
	current_time = THRESHOLD * k;
	}
	return;
}
