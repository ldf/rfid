/**CFile*****************************************************************

  FileName    [node.c]

  Synopsis    [Implementation file for the "node" data structure.]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "node.h"
#include "utility.h"

#include <math.h>

/**Function********************************************************************
  Synopsis           [Initialize node data structure.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int node_init(node_t **n){
	(*n) = (node_t *)malloc(sizeof(node_t));
	if((*n)==NULL) {fprintf(stderr, "Malloc failed in `param_init'\n"); return -1;}
	coord_init(&((*n)->coord));
	move_init(&((*n)->move));
	// Default state is ON
	(*n)->state = ON;
	(*n)->time_on_off = 0.0;

	return 0;
}

/**Function********************************************************************
  Synopsis           [Update the condition of all nodes in the map]
  Description        [This function perform the update considering the starting
  	  	  	  	  	  time as base and `float time' variable as offset.]
  SideEffects        []
  SeeAlso            []
 ******************************************************************************/
int node_update(random_t *r, node_t **n, float time){
	double space, angle;
	float xp, yp, state_time;

	int recursive = 1;


	/* Determining weather the node is ON or OFF */
	if(r->time_off_min >= 0 && r->time_off_max >= 0){
		// State does NOT change
		if ((*n)->time_on_off >= time){
			(*n)->time_on_off -= time;
		}else{
			// A new state must be generated
			state_time = time;
			while((*n)->time_on_off < state_time){
				state_time -= (*n)->time_on_off;
				node_set_time_on_off(r, n);
				if ((*n)->time_on_off >= time){
					(*n)->time_on_off -= time;
				}
			}
		}
	}
	/* Set up the new coord and move */
	while(recursive == 1){
		recursive = 0;
		/* Case 0: the node is still waiting his wait_time before moving	*/
		if((*n)->move->wait_time >= time) (*n)->move->wait_time -= time;
		/* Case 1: the node is moving 										*/
		else {
			time -= (*n)->move->wait_time;
			(*n)->move->wait_time = 0;
			space = (*n)->move->speed * time;
			angle = (float)atan2((*n)->move->coord->y - (*n)->coord->y, (*n)->move->coord->x - (*n)->coord->x);
			xp = space * (float)cos(angle) + (*n)->coord->x;
			yp = space * (float)sin(angle) + (*n)->coord->y;
			/* Checking for error */
//			if( abs(((*n)->coord->x-xp)/((*n)->move->coord->x-xp) - ((*n)->coord->y-yp)/((*n)->move->coord->y-yp)) > 0.00001){
//				fprintf(stderr, "ERROR\n");
//				fprintf(stderr, "%f =? %f\n",
//						((*n)->coord->x-xp)/((*n)->move->coord->x-xp),
//						((*n)->coord->y-yp)/((*n)->move->coord->y-yp));
//			}

			/* Case 1,A: the node has NOT reached his target point yet		*/
			if( fabs(xp - (*n)->coord->x) < fabs(xp - (*n)->move->coord->x) ){
				(*n)->coord->x = xp;
				(*n)->coord->y = yp;
				/* Case 1,B: the node HAS reached his target point (maybe it has surpassed the target point)	*/
			}else{
				/* The node reachs the target point */
				(*n)->coord->x = (*n)->move->coord->x;
				(*n)->coord->y = (*n)->move->coord->y;
				/* Calculating the time that he might has spent `surpassing the target point'*/
				space = fabs(yp - (*n)->coord->y) * sin(angle);
				time = space / (*n)->move->speed;
				/* The node uses this time to perform the next move */
				node_set_move(r, n);
				//			node_update(r, n, time);
				recursive = 1;
			}
		}
	}
	node_set_tag_distance(r, n);
	return 0;
}

/**Function********************************************************************
  Synopsis           [Set a coordinate for a node.]
  Description        []
  SideEffects        []
  SeeAlso            []
 ******************************************************************************/
int node_set_coord(random_t *r, node_t **n){
	random_coord(r, &((*n)->coord), &((*n)->seed_position));
	return 0;
}

/**Function********************************************************************
  Synopsis           [Set a move for a node.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int node_set_move(random_t *r, node_t **n){
	random_coord(r, &((*n)->move->coord), &((*n)->seed_position));
	random_speed(r, &((*n)->move->speed), &((*n)->seed_speed));
	random_wait_time(r, &((*n)->move->wait_time), &((*n)->seed_wait_time));
	return 0;
}

/**Function********************************************************************
  Synopsis           [Set a move for a node.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int node_set_tag_distance(random_t *r, node_t **n){
	random_tag_distance(r, &((*n)->tag_distance), &((*n)->seed_tag_distance));
	return 0;
}

/**Function********************************************************************
  Synopsis           [Print node parameters.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
void node_print(node_t *n){
	coord_print(n->coord);
	move_print(n->move);
	return;
}

/**Function********************************************************************
  Synopsis           [Set the time and the state of a node.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int node_set_time_on_off(random_t *r, node_t **n){
	random_time_on_off(r, &((*n)->time_on_off), &((*n)->state), &((*n)->seed_time_off), &((*n)->seed_state));
	return 0;
}
