/**CFile*****************************************************************

  FileName    [utility.c]

  Synopsis    [Implementation file for the "utility" function(s).]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "utility.h"

/**Function********************************************************************
  Synopsis           [Allocate a pointer and check for errors.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int Malloc(void **p, int size){
	(*p) = malloc(size);
	if((*p)==NULL) {fprintf(stderr, "MALLOC FAIL\n"); return -1;}
	return 0;
}
