/**CFile*****************************************************************

  FileName    [inter.c]

  Synopsis    [Implementation file for the "inter" data structure.]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "inter.h"

#include <sys/time.h>


typedef struct cl_node{
	float x;
	float y;
	float tx;
	float ty;
	float speed;
	float wait_time;
	float time_on_off;
	float state;
	float tag_distance;
} cl_node_t;

typedef struct cl_param{
	float x, y;
	int num_dev, num_it;
	int model_on_off;
	float gain, gain_2, lambda, lambda_2, threshold, sim_time;
} cl_param_t;

typedef struct cl_random{
	float wait_time_min, wait_time_max;
	float position_x, position_y;
	float speed_min, speed_max;
	float time_off_min, time_off_max;
	float tag_distance_min, tag_distance_max;
} cl_random_t;

struct timeval serial;
struct timeval parallel;
struct timeval parallel2;

int inter_init(inter_t **i){
	Malloc((void *)i, sizeof(inter_t));
	return 0;
}
int inter_first_setup(inter_t **i, int num_dev){
	int k;
	Malloc((void *)&((*i)->v), num_dev*sizeof(int));
	for(k=0; k<num_dev; k++) {
		(*i)->v[k] = ON;
	}
	(*i)->num_dev = num_dev;
	return 0;
}

float inter_dist_a_b(float x0, float x1, float y0, float y1){
	float dist_2, angle = atan2(y1-y0, x1-x0);
	dist_2 = fabs((y1-y0)/sin(angle));
	return dist_2*dist_2;
}

int inter_update(map_t *m, inter_t **i){
	struct timeval start;
	gettimeofday(&start, NULL);

	float iv, dist_2;
	int k, j;

	float *x = (float*) malloc(m->p->num_dev*(sizeof(float)));

	for(k=0; k<m->p->num_dev; k++){
		(*i)->v[k]=0;
		x[k] = 0;
	}

	for(k=0; k<m->p->num_dev; k++){
		if(m->nodes[k]->state == ON){
			for(j=0; j<m->p->num_dev; j++){
				if(m->nodes[j]->state == ON){
					if(j!=k){
						dist_2 =inter_dist_a_b(m->nodes[k]->coord->x, m->nodes[j]->coord->x,
								m->nodes[k]->coord->y, m->nodes[j]->coord->y);
						iv = m->p->gain_2 * m->p->lambda_2 / dist_2 / 12.56636;
						x[k] += iv;
						if(iv > m->p->threshold) {
							(*i)->v[k]=1;
							(*i)->v[j]=1;
						}
					}
				}

			}
		}
		dist_2 = m->nodes[k]->tag_distance * m->nodes[k]->tag_distance;
		iv = m->p->gain_2 * m->p->lambda_2 / dist_2 / 12.56636;
		x[k] = iv / x[k];
//		printf("%f ", x[k]);

	}
//	printf("\n");

	struct timeval end;
	gettimeofday(&end, NULL);
//	printf("time = %u.%06u\n",
//			end.tv_sec-start.tv_sec, end.tv_usec-start.tv_usec);
	serial.tv_sec = end.tv_sec-start.tv_sec;
	serial.tv_usec = end.tv_usec-start.tv_usec;


	return 0;
}

void inter_print(inter_t *i){
	int k;
//	if(VERBOSITY>-1){
//		for(k=0; k<i->num_dev; k++){
//			fprintf(stdout, "%3d ", i->v[k]);
//		}
//		fprintf(stdout, "\n");
//	}
}

#define MAX_SOURCE_SIZE (0x100000)
#define NUM_SEED 6

int cl_inter_update(map_t *m, inter_t **inter){
	struct timeval start;
	gettimeofday(&start, NULL);
	cl_platform_id platform_id = NULL;
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_mem Cmobj = NULL;
	cl_mem Pmobj = NULL;
	cl_mem Rmobj = NULL;
	cl_mem Smobj = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;

	int i;
	float *C;
	cl_param_t *cl_param;
	cl_random_t *cl_random;
	int *cl_seeds;


	FILE *fp;
	const char fileName[] = "./inter.cl";
	size_t source_size;
	char *source_str;

	/* Load kernel source file */
	fp = fopen(fileName, "r");
	if (!fp) {
		fprintf(stderr, "Failed to load kernel.\n");
		exit(1);
	}
	source_str = (char *)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);

	/* Initialize input data */

	C = (float *)malloc(m->p->num_it*m->p->num_dev*sizeof(float));
	cl_param = (cl_param_t *)malloc(sizeof(cl_param_t));
	cl_random = (cl_random_t *)malloc(sizeof(cl_random_t));
	cl_seeds = (int *)malloc(NUM_SEED*m->p->num_dev*sizeof(int));


	cl_random->position_x = m->r->position_x;
	cl_random->position_y = m->r->position_y;
	cl_random->speed_min = m->r->speed_min;
	cl_random->speed_max = m->r->speed_max;
	cl_random->wait_time_min = m->r->wait_time_min;
	cl_random->wait_time_max = m->r->wait_time_max;
	cl_random->time_off_min = m->r->time_off_min;
	cl_random->time_off_max = m->r->time_off_max;
	cl_random->tag_distance_min = m->r->tag_distance_min;
	cl_random->tag_distance_max = m->r->tag_distance_max;

	cl_param->gain = m->p->gain;
	cl_param->gain_2 = m->p->gain_2;
	cl_param->lambda = m->p->lambda;
	cl_param->lambda_2 = m->p->lambda_2;
	cl_param->num_it = m->p->num_it;
	cl_param->num_dev = m->p->num_dev;

	int a, b, c;
	a = 1103515245;
	b = 12345;
	c = 2147483647;
	for(i=0; i<m->p->num_dev; i++){
		cl_seeds[0*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_position);
		cl_seeds[1*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_speed);
		cl_seeds[2*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_state);
		cl_seeds[3*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_time_off);
		cl_seeds[4*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_wait_time);
		cl_seeds[5*m->p->num_dev + i] = lcrg_seed(a, b, c, i, m->r->seed_tag_distance);
	}
	printf("\n");

	/* Get Platform/Device Information */
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);
	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);
	cl_char device_name[1024] = {0};
	ret = clGetDeviceInfo(device_id, CL_DEVICE_NAME, sizeof(device_name), &device_name, NULL);
	size_t max_work_group_size;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_work_group_size), &max_work_group_size, NULL);
	cl_uint max_work_item_dimensions;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(max_work_item_dimensions), &max_work_item_dimensions, NULL);
	size_t max_work_item_size[3];
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_size), &max_work_item_size, NULL);

	/* Create OpenCL Context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);

	/* Create command queue */
	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

	/* Create Buffer Object */
	Cmobj = clCreateBuffer(context, CL_MEM_READ_WRITE, m->p->num_it*cl_param->num_dev*sizeof(float), NULL, &ret);
	Pmobj = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_param_t), NULL, &ret);
	Rmobj = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_random_t), NULL, &ret);
	Smobj = clCreateBuffer(context, CL_MEM_READ_ONLY, NUM_SEED*cl_param->num_dev*sizeof(int), NULL, &ret);

	/* Copy input data to the memory buffer */
	ret = clEnqueueWriteBuffer(command_queue, Pmobj, CL_TRUE, 0, sizeof(cl_param_t), cl_param, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, Rmobj, CL_TRUE, 0, sizeof(cl_random_t), cl_random, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, Smobj, CL_TRUE, 0, NUM_SEED*cl_param->num_dev*sizeof(int), cl_seeds, 0, NULL, NULL);

	/* Create kernel program from source file*/
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if(ret !=0) printf("BUILD: %d\n", ret);
	/* Create data parallel OpenCL kernel */
	kernel = clCreateKernel(program, "dataParallel", &ret);

	/* Set OpenCL kernel arguments */
	ret = clSetKernelArg(kernel, 0, cl_param->num_dev*sizeof(cl_node_t), NULL);
	if(ret !=0) printf("0: %d\n", ret);
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&Smobj);
	if(ret !=0) printf("1: %d\n", ret);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&Cmobj);
	if(ret !=0) printf("2: %d\n", ret);
	ret = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&Pmobj);
	if(ret !=0) printf("3: %d\n", ret);
	ret = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&Rmobj);
	if(ret !=0) printf("4: %d\n", ret);

	size_t global_item_size = cl_param->num_dev;
	size_t local_item_size = 1; //Not actually used

	struct timeval beforeL;
	gettimeofday(&beforeL, NULL);
//	printf("time = %u.%06u\n",
//			beforeL.tv_sec-start.tv_sec, beforeL.tv_usec-start.tv_usec);


	/* Execute OpenCL kernel as data parallel */
	ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
			&global_item_size, NULL, 0, NULL, NULL);
	printf("enqueueND: %d\n", ret);



	struct timeval afterL;
	gettimeofday(&afterL, NULL);
//	printf("time = %u.%06u\n",
//			afterL.tv_sec-start.tv_sec, afterL.tv_usec-start.tv_usec);

	/* Transfer result to host */
	ret = clEnqueueReadBuffer(command_queue, Cmobj, CL_TRUE, 0, m->p->num_it*cl_param->num_dev*sizeof(float), C, 0, NULL, NULL);
	if(ret !=0) printf("dev->host: %d\n", ret);

	/* Display Results */
//	int j;
//	for(j=0; j<cl_param->num_it; j++){
//		for(i=0; i<cl_param->num_dev; i++){
//			printf("%f ", C[j*cl_param->num_dev+i]);
//		}
//		printf("\n");
//	}
	/* Finalization */
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(Cmobj);
	ret = clReleaseMemObject(Pmobj);
	ret = clReleaseMemObject(Rmobj);
	ret = clReleaseMemObject(Smobj);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);

	free(source_str);

	free(C);
	free(cl_param);
	free(cl_random);
	free(cl_seeds);

	struct timeval end;
	gettimeofday(&end, NULL);
//	printf("time = %u.%06u\n",
//			end.tv_sec-start.tv_sec, end.tv_usec-start.tv_usec);
	parallel.tv_sec = end.tv_sec-start.tv_sec;
	parallel.tv_usec = end.tv_usec-start.tv_usec;

	struct timeval cmp;
	cmp.tv_sec = parallel.tv_sec-serial.tv_sec;
	if(cmp.tv_sec <= 0){
		cmp.tv_usec = serial.tv_usec-parallel.tv_usec;
//		printf("Diff = %d\n",
//			cmp.tv_usec);
	}
	parallel2.tv_sec = end.tv_sec-start.tv_sec;
	parallel2.tv_usec = end.tv_usec-start.tv_usec;

	cmp.tv_sec = parallel2.tv_sec-serial.tv_sec;
	if(cmp.tv_sec <= 0){
		cmp.tv_usec = serial.tv_usec-parallel2.tv_usec;
//		printf("Diff = %d\n",
//				cmp.tv_usec);
	}
	return 0;
}
