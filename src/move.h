/**CHeaderFile*****************************************************************

  FileName    [move.h]�

  Synopsis    [Header file for the "move" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef MOVE_H_
#define MOVE_H_

/*---------------------------------------------------------------------------*/
/* Nested includes                                                           */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "coord.h"
#include "random.h"
#include "utility.h"

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef struct move move_t;

/* A move is associated to a given node. The node wants to give toward a certain
 * point [coord->x, coord->y] in the map with certain parameters [speed, wait_time,
 * etc.] */
struct move{
	/* Coordinates of the target point, the one that the given node wants to reach*/
	coord_t *coord;
	float speed;
	/* Time that the given nodes has to wait before it begins to move */
	float wait_time;
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
int move_init(move_t **m);
void move_print(move_t *m);

#endif /* RMOV_H_ */
