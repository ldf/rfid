/**CHeaderFile*****************************************************************

  FileName    [node.h]�

  Synopsis    [Header file for the "node" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef NODE_H_
#define NODE_H_

/*---------------------------------------------------------------------------*/
/* Nested includes                                                           */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "coord.h"
#include "move.h"
#include "random.h"

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef struct node node_t;

struct node{
	coord_t *coord;
	move_t *move;
	float time_on_off;
	float tag_distance;
	int state;
	int seed_wait_time;
	int seed_position;
	int seed_speed;
	int seed_time_off;
	int seed_state;
	int seed_tag_distance;
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/

int node_init(node_t **n);
int node_set_move(random_t *r, node_t **n);
int node_set_coord(random_t *r, node_t **n);
int node_set_time_on_off(random_t *r, node_t **n);
int node_set_tag_distance(random_t *r, node_t **n);
int node_update(random_t *r, node_t **n, float time);
void node_print(node_t *n);

#endif /* NODE_H_ */
