/*
 * random.h
 *
 *  Created on: Nov 23, 2013
 *      Author: ldf
 */

#ifndef RANDOM_H_
/**CHeaderFile*****************************************************************

  FileName    [node.h]�

  Synopsis    [Header file for the "node" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#define RANDOM_H_

/*---------------------------------------------------------------------------*/
/* Nested includes                                                           */
/*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coord.h"
#include "move.h"
#include "utility.h"
/* This is the external lgpl library for generating uniform random distributed float */
#include "uniform.h"

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef struct random random_t;

struct random{
	int seed_wait_time;
	float wait_time_min, wait_time_max;
	int seed_position;
	float position_x, position_y;
	int seed_speed;
	float speed_min, speed_max;
	int seed_time_off;
	float time_off_min, time_off_max;
	int seed_state;
	float tag_distance_min, tag_distance_max;
	int seed_tag_distance;
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
int random_init(random_t **r);
int random_input_file_read(random_t **r, int model_on_off);
float random_uniform_float_gen(float min, float max, int *seed);
void random_coord(random_t *r, coord_t **c, int *seed);
void random_speed(random_t *r, float *speed, int *seed);
void random_wait_time(random_t *r, float *wait_time, int *seed);
void random_tag_distance(random_t *r, float *tag_distance, int *seed);
void random_time_on_off(random_t *r, float *wait_time, int *state, int *seed_time_on_off, int *seed_state);
void random_print(random_t *r);

#endif /* RANDOM_H_ */
