/*
 * coord.c
 *
 *  Created on: Nov 23, 2013
 *      Author: ldf
 */

#include "coord.h"

#include <stdio.h>
#include <stdlib.h>

int coord_init(coord_t **c){
	(*c) = (coord_t *)malloc(sizeof(coord_t));
	return 0;
}

void coord_print(coord_t *c){
	fprintf(stdout, "x=%f y=%f\n", c->x, c->y);
	return;
}
