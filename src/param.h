/**CHeaderFile*****************************************************************

  FileName    [param.h]�

  Synopsis    [Header file for the "param" data structure]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#ifndef PARAM_H_
#define PARAM_H_

/*---------------------------------------------------------------------------*/
/* Nested includes                                                           */
/*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coord.h"
#include "param.h"

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

typedef struct param param_t;

struct param{
  coord_t *coord;
  int num_dev, num_it;
  int model_on_off;
  float gain, gain_2, lambda, lambda_2, threshold, sim_time;
  char *move_model, *equation_model;
};

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/

int param_init(param_t **p);
void param_print(param_t *p);

#endif /* PARAM_H_ */
