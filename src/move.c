/*
 * move.c
 *
 *  Created on: Nov 22, 2013
 *      Author: ldf
 */

#include "move.h"

//TODO checking the malloc
int move_init(move_t **m){
	Malloc((void *)m, sizeof(move_t));
	//(*m)->coord = (coord_t *)malloc(sizeof(coord_t));
	coord_init(&((*m)->coord));
	return 0;
}

void move_print(move_t *m){
	fprintf(stdout, "move print: ");
	coord_print(m->coord);
	fprintf(stdout, "speed: %f\n", m->speed);
	fprintf(stdout, "wait_time: %f\n", m->wait_time);
	return;
}
