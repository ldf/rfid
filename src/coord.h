/*
 * coord.h
 *
 *  Created on: Nov 23, 2013
 *      Author: ldf
 */

#ifndef COORD_H_
#define COORD_H_

typedef struct coord coord_t;

struct coord{
	float x, y;
};

int coord_init(coord_t **c);
void coord_print(coord_t *c);

#endif /* COORD_H_ */
