/**CFile*****************************************************************

  FileName    [map.c]

  Synopsis    [Implementation file for the "map" data structure.]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "map.h"

#define MAXBUF 40


/**Function********************************************************************
  Synopsis           [Initialize map data structure.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int map_init(map_t **m){
	Malloc((void *)m, sizeof(map_t));
	param_init(&((*m)->p));
	return 0;
}

/**Function********************************************************************
  Synopsis           [Read the input file and sets the parameters.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int map_input_file_read(map_t **m){
  FILE *ifp; //i=input : fp: file pointer
  char *ifn; //i=input : fn: file name
  char buf[MAXBUF+1], parser[MAXBUF+1];

  ifn = strdup("/Users/ldf/Documents/workspace/rfid/src/etc/config"); //TODO this should be better setted
  ifp = fopen(ifn, "r");
  if(ifp==NULL) {fprintf(stderr, "No config file has been found in ./etc/ \n"); return -1;}
  while(fgets(buf, MAXBUF, ifp)!=NULL){
    sscanf(buf, "%s", parser);
    switch(buf[0]) {
    case 'd':
      if(!strcmp(parser, "dev")) sscanf(buf, "%*s %d", &((*m)->p->num_dev));
      break;
    case 'e':
      if(!strcmp(parser, "equation_model")){
    	  sscanf(buf, "%*s %s", parser);
    	  (*m)->p->equation_model=strdup(parser);
      }
      break;
    case 'g':
      if(!strcmp(parser, "gain")) {
    	  sscanf(buf, "%*s %f", &((*m)->p->gain));
    	  (*m)->p->gain_2 = (*m)->p->gain * (*m)->p->gain;
      }
      break;
    case 'i':
      if(!strcmp(parser, "iteration")) sscanf(buf, "%*s %d", &((*m)->p->num_it));
      break;
    case 'l':
      if(!strcmp(parser, "lambda")){
    	  sscanf(buf, "%*s %f", &((*m)->p->lambda));
    	  (*m)->p->lambda_2 = (*m)->p->lambda * (*m)->p->lambda;
      }
      break;
    case 'm':
      if(!strcmp(parser, "map")) sscanf(buf, "%*s %f %f", &((*m)->p->coord->x), &((*m)->p->coord->y));
      else if(!strcmp(buf, "move_model")) {
    	  sscanf(buf, "%*s %s", parser);
    	  (*m)->p->move_model=strdup(parser);
      }
      else if(!strcmp(buf, "model_on_off")) {
          	  sscanf(buf, "%*s %d", &((*m)->p->model_on_off));
            }
      break;
    case 's':
      if(!strcmp(parser, "simulation_time")) sscanf(buf, "%*s %f", &((*m)->p->sim_time));
      break;
    case 't':
      if(!strcmp(parser, "threshold")) sscanf(buf, "%*s %f", &((*m)->p->threshold));
      break;
    }
  }
  fclose(ifp);
  return 0;
}
/**Function********************************************************************
  Synopsis           [Set up for all the nodes in the map]
  Description        [This function has to be called as soon as the map is
  	  	  	  	  	  create. Nodes are given a position and a target move.]
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int map_first_setup(random_t *r, map_t **m){
	int i;
	int a, b, c;
	a = 1103515245;
	b = 12345;
	c = 2147483647;
	Malloc((void *)&((*m)->nodes), (*m)->p->num_dev*sizeof(node_t *));
	(*m)->r = r;
	for(i=0; i<(*m)->p->num_dev; i++){
		node_init(&((*m)->nodes[i]));

		(*m)->nodes[i]->seed_position = lcrg_seed(a, b, c, i, r->seed_position);
		(*m)->nodes[i]->seed_speed = lcrg_seed(a, b, c, i, r->seed_speed);
		(*m)->nodes[i]->seed_state = lcrg_seed(a, b, c, i, r->seed_state);
		(*m)->nodes[i]->seed_time_off = lcrg_seed(a, b, c, i, r->seed_time_off);
		(*m)->nodes[i]->seed_wait_time = lcrg_seed(a, b, c, i, r->seed_wait_time);
		(*m)->nodes[i]->seed_tag_distance = lcrg_seed(a, b, c, i, r->seed_tag_distance);


		node_set_coord(r, &((*m)->nodes[i]));
		node_set_move(r, &((*m)->nodes[i]));
		node_set_time_on_off(r, &((*m)->nodes[i]));
		(*m)->nodes[i]->state = 1;
		node_set_tag_distance(r, &((*m)->nodes[i]));
	}
	return 0;
}
/**Function********************************************************************
  Synopsis           [Update the condition of all nodes in the map]
  Description        [This function perform the update considering the starting
  	  	  	  	  	  time as base and `float time' variable as offset.]
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int map_update(random_t *r, map_t **m, float time){
	int i;
	for(i=0; i<(*m)->p->num_dev; i++){
		node_update(r, &((*m)->nodes[i]), time);
	}
	return 0;
}

/**Function********************************************************************
  Synopsis           [Print the parameters of the entire map.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int map_print(map_t *m){
	int i;
	for(i=0; i<m->p->num_dev; i++){
		fprintf(stdout, "%d\n", i);
		node_print(m->nodes[i]);
	}

	return 0;
}
