/**CHeaderFile*****************************************************************

  FileName    [main.c]�

  Synopsis    []

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "param.h"
#include "map.h"
#include "random.h"
#include "move.h"
#include "uniform.h"
#include "inter.h"
#include "visual.h"

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/OpenCL>
#endif



int main (int argc, char *argv[]){
  random_t *r; random_init(&r);
  map_t *m; map_init(&m);
  inter_t *i; inter_init(&i);
  int iteration;

  struct timeval s_start, s_stop, p_start, p_stop;


  if(VERBOSITY>1) fprintf(stdout, "-- STARTING MAIN --\n");

  /* Gathering values */
  map_input_file_read(&m);
  if(VERBOSITY>4) 	param_print(m->p);
  random_input_file_read(&r, m->p->model_on_off);
  	  /* check SideEffects of this fuction */
  	  if(r->position_x==0 && r->position_y==0) { r->position_x=m->p->coord->x; r->position_y=m->p->coord->y;}
  if(VERBOSITY>4) 	random_print(r);

  /* Initializing nodes in the map and the interference */
  map_first_setup(r, &m);
  inter_first_setup(&i, m->p->num_dev);
  if(VERBOSITY>1) 	map_print(m);

  /* Set up global parameters */
  m->r=r;
  global_map = m;
  global_inter=i;
  /* Starting the simulation */
  if(VISUAL==YES){
	  /* Set up of the graphical window */
	  glutInit(&argc, argv);
	  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA);
	  glutInitWindowSize(m->p->coord->x,m->p->coord->y);
	  glutCreateWindow("Map");
	  glutDisplayFunc(&visual_render);
	  glutReshapeFunc(visual_reshape);
	  glutIdleFunc(visual_simulation);
	  glEnable(GL_POINT_SMOOTH);
	  /* Launch the graphical window */
	  glutMainLoop();
  }else if(VISUAL==NO){
	  /* Simulation without the graphical window */
	  gettimeofday(&s_start, NULL);
	  iteration = m->p->num_it;
	  inter_update(global_map, &(global_inter));
	  while(iteration-- > 0){
		  map_update(global_map->r, &global_map, global_map->p->sim_time);
		  if(VERBOSITY>1)	 map_print(global_map);
		  inter_update(global_map, &(global_inter));
		  if(VERBOSITY>-1)	 inter_print(global_inter);
	  }
	  gettimeofday(&s_stop, NULL);

	  gettimeofday(&p_start, NULL);
	  cl_inter_update(m, &i);
	  gettimeofday(&p_stop, NULL);

	  printf("time = %u.%06u\n",
			  s_stop.tv_sec-s_start.tv_sec, s_stop.tv_usec-s_start.tv_usec);
	  printf("time = %u.%06u\n",
			  p_stop.tv_sec-p_start.tv_sec, p_stop.tv_usec-p_start.tv_usec);

  }
  if(VERBOSITY>1) 	fprintf(stdout, "-- END MAIN --\n");

  return 0;
}
