/**CFile*****************************************************************

  FileName    [param.c]

  Synopsis    [Implementation file for the "param" data structure.]

  Description []

  SeeAlso     []

  Author      [Lorenzo David]

******************************************************************************/

#include "param.h"

/**Function********************************************************************
  Synopsis           [Initialize node data structure.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
int param_init(param_t **p){
  (*p) = (param_t *)malloc(sizeof(param_t));
  if((*p)==NULL) {fprintf(stderr, "Malloc failed in `param_init'\n"); return -1;}
  (*p)->coord = (coord_t *)malloc(sizeof(coord_t));
  return 0;
}

/**Function********************************************************************
  Synopsis           [Print node parameters.]
  Description        []
  SideEffects        []
  SeeAlso            []
******************************************************************************/
void param_print(param_t *p){
  fprintf(stdout, "Parameters configured:\n");
  fprintf(stdout, "map %.2f %.2f\n", p->coord->x, p->coord->y);
  fprintf(stdout, "dev %d\n", p->num_dev);
  fprintf(stdout, "gain %.2f\n", p->gain);
  fprintf(stdout, "End of parameters\n\n");
}
