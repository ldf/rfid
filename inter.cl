# define N_MAX 100
# define CONST 12.56636

typedef struct cl_node{
	float x;
	float y;
	float tx;
	float ty;
	float speed;
	float wait_time;
	float time_on_off;
	float state;
	float tag_distance;
} cl_node_t;

typedef struct cl_random{
	float wait_time_min, wait_time_max;
	float position_x, position_y;
	float speed_min, speed_max;
	float time_off_min, time_off_max;
	float tag_distance_min, tag_distance_max;
} cl_random_t;

typedef struct cl_seed{
	int seed_x;
} cl_seed_t;

typedef struct cl_param{
	float x, y;
	int num_dev, num_it;
	int model_on_off;
	float gain, gain_2, lambda, lambda_2, threshold, sim_time;
} cl_param_t;


constant int i4_huge = 2147483647;
float r4_uniform_ab ( float a, float b, int *seed ){

  int k;
  private float value;

  k = *seed / 127773;

  *seed = 16807 * ( *seed - k * 127773 ) - k * 2836;

  if ( *seed < 0 )
  {
    *seed = *seed + i4_huge;
  }

  value = ( float ) ( *seed ) * 4.656612875E-10;

  value = a + ( b - a ) * value;

  return value;
}

void update_seed ( local float *a, local float *b, local int *seed ){
	int k;
	k = (*seed) / 127773;
	(*seed) = 16807 * ( (*seed) - k * 127773 ) - k * 2836;
	if ( (*seed) < 0 ){
		(*seed) = (*seed) + i4_huge;
  	}
	return;
}

float float_uniform_ab ( private float a, private float b, private int seed ){
	float value = ( float ) seed * 4.656612875E-10;
	value = a + (b - a) * value;
	return value;
}

__kernel void
dataParallel(local cl_node_t *reader, __global int *seeds, __global float *OUT, __constant cl_param_t *param, __constant cl_random_t *random){
	/*First work initialize the environment*/
		
	//1st thread in each workgroup initializes local buffer
	//if(get_local_id(0) == 0){
	//}
	//wait for all workgroups to finish accessing any memory
	//barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);

	//cl_uniform_ab(0, random->position_x, random->seed_position, &(tmp[j]));

	int i = 0;
	int j = get_global_id(0);
	int k = 0;
	private float value, tmp;
	private float time = 5;
	
	private int private_seed_wait_time = 0;
	private int private_seed_position = 0;
	private int private_seed_speed = 0;
	private int private_seed_time_off = 0;
	private int private_seed_state = 0;
	private int private_seed_tag_distance = 0;
	
	private float space, angle;
	private float xp, yp, state_time;
	private int recursive = 1;
	
	private_seed_position = seeds[0*param->num_dev + j];
	private_seed_speed = seeds[1*param->num_dev + j];
	private_seed_state = seeds[2*param->num_dev + j];
	private_seed_time_off = seeds[3*param->num_dev + j];
	private_seed_wait_time = seeds[4*param->num_dev + j];
	private_seed_tag_distance = seeds[5*param->num_dev + j];
			
	reader[j].x = r4_uniform_ab(0.0, random->position_x, &private_seed_position);
	reader[j].y = r4_uniform_ab(0.0, random->position_y, &private_seed_position);
	reader[j].tx = r4_uniform_ab(0.0, random->position_x, &private_seed_position);
	reader[j].ty = r4_uniform_ab(0.0, random->position_y, &private_seed_position);
	reader[j].wait_time = r4_uniform_ab(random->wait_time_min, random->wait_time_max, &private_seed_wait_time);
	reader[j].speed = r4_uniform_ab(random->speed_min, random->speed_max, &private_seed_speed);
	reader[j].time_on_off = r4_uniform_ab(random->time_off_min, random->time_off_max, &private_seed_time_off);
	
	reader[j].tag_distance = r4_uniform_ab(random->tag_distance_min, random->tag_distance_max, &private_seed_tag_distance);
	reader[j].state = 1;
	
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	
	for(k=0; k<param->num_it; k++){

		OUT[k*param->num_dev+j] = 0;
		for(i=0; i<param->num_dev; i++){
			/* Calculating the interference */
			if(i!=j){
				// calculate the distance Ri-Rj
				value = fabs((reader[j].y-reader[i].y)/sin(atan2(reader[j].y-reader[i].y, reader[j].x-reader[i].x)));
				value *=value;
				// calculate the friis function for Ri(Ti)-Rj == Rj(Tj)-Ri
				value = param->gain_2 * param->lambda_2 / value / 12.56636;
				OUT[k*param->num_dev+j] += value;	
			}
		}
		tmp = reader[j].tag_distance * reader[j].tag_distance;
		tmp = param->gain_2 * param->lambda_2 / tmp / 12.56636;
		OUT[k*param->num_dev+j] = tmp / OUT[k*param->num_dev+j];
				
		barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
		/* Update */
		recursive = 1;
		time = 5;
		reader[j].tag_distance = r4_uniform_ab(random->tag_distance_min, random->tag_distance_max, &private_seed_tag_distance);
		/* Determining weather the node is ON or OFF */
		if(random->time_off_min >= 0 && random->time_off_max >= 0){
			// State does NOT change
			if (reader[j].time_on_off >= time){
				reader[j].time_on_off -= time;
			}else{
				// A new state must be generated
				state_time = time;
				while(reader[j].time_on_off < state_time){
					state_time -= reader[j].time_on_off;
					// SET time_on_off
					reader[j].time_on_off = r4_uniform_ab(random->time_off_min, random->time_off_max, &private_seed_time_off);
					if (reader[j].time_on_off >= time){
						reader[j].time_on_off -= time;
					}
				}
			}
		}
		/* Set up the new coord and move */
		while(recursive == 1){
			recursive = 0;
			/* Case 0: the node is still waiting his wait_time before moving	*/
			if(reader[j].wait_time >= time) reader[j].wait_time -= time;
			/* Case 1: the node is moving 										*/
			else {
				time -= reader[j].wait_time;
				reader[j].wait_time = 0;
				space = reader[j].speed * time;
				angle = (float)atan2(reader[j].ty - reader[j].y, reader[j].tx - reader[j].x);
				xp = space * (float)cos(angle) + reader[j].x;
				yp = space * (float)sin(angle) + reader[j].y;

				/* Case 1,A: the node has NOT reached his target point yet		*/
				if( fabs(xp - reader[j].x) < fabs(xp - reader[j].tx) ){
					reader[j].x = xp;
					reader[j].y = yp;
					/* Case 1,B: the node HAS reached his target point (maybe it has surpassed the target point)	*/
				}else{
					/* The node reachs the target point */
					reader[j].x = reader[j].tx;
					reader[j].y = reader[j].ty;
					/* Calculating the time that he might has spent `surpassing the target point'*/
					space = fabs(yp - reader[j].y) * sin(angle);
					time = space / reader[j].speed;
					/* The node uses this time to perform the next move */
					// SET MOVE
					reader[j].tx = r4_uniform_ab(0.0, random->position_x, &private_seed_position);
					reader[j].ty = r4_uniform_ab(0.0, random->position_y, &private_seed_position);
					reader[j].wait_time = r4_uniform_ab(random->wait_time_min, random->wait_time_max, &private_seed_wait_time);
					reader[j].speed = r4_uniform_ab(random->speed_min, random->speed_max, &private_seed_speed);			
					recursive = 1;
				}
			}
		}
		barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	}

}